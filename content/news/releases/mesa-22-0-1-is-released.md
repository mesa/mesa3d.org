---
title:    "Mesa 22.0.1 is released"
date:     2022-03-29
category: releases
tags:     []
---
[Mesa 22.0.1](https://docs.mesa3d.org/relnotes/22.0.1.html) is released.
This is a bug fix release.
