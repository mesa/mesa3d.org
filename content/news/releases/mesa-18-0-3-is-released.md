---
title:    "Mesa 18.0.3 is released"
date:     2018-05-07
category: releases
tags:     []
---
[Mesa 18.0.3](https://docs.mesa3d.org/relnotes/18.0.3.html) is released. This is a bug-fix
release.
