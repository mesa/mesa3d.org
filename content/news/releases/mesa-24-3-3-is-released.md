---
title:    "Mesa 24.3.3 is released"
date:     2025-01-03
category: releases
tags:     []
---
[Mesa 24.3.3](https://docs.mesa3d.org/relnotes/24.3.3.html) is released.
This is a bug fix release.
