---
title:    "Mesa 6.5.3 is released"
date:     2007-04-27
category: releases
tags:     []
---
[Mesa 6.5.3](https://docs.mesa3d.org/relnotes/6.5.3.html) is released. This is a development
release which will lead up to the Mesa 7.0 release (which will advertise
OpenGL 2.1 API support).
