---
title:    "Mesa 17.2.1 is released"
date:     2017-09-17
category: releases
tags:     []
---
[Mesa 17.2.1](https://docs.mesa3d.org/relnotes/17.2.1.html) is released. This is a bug-fix
release.
