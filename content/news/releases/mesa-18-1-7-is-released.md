---
title:    "Mesa 18.1.7 is released"
date:     2018-08-24
category: releases
tags:     []
---
[Mesa 18.1.7](https://docs.mesa3d.org/relnotes/18.1.7.html) is released. This is a bug-fix
release.
