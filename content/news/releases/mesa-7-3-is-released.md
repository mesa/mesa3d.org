---
title:    "Mesa 7.3 is released"
date:     2009-01-22
category: releases
tags:     []
---
[Mesa 7.3](https://docs.mesa3d.org/relnotes/7.3.html) is released. This is a new development
release. Mesa 7.4 will follow and will have bug fixes relative to 7.3.
