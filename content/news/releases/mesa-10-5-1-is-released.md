---
title:    "Mesa 10.5.1 is released"
date:     2015-03-13
category: releases
tags:     []
---
[Mesa 10.5.1](https://docs.mesa3d.org/relnotes/10.5.1.html) is released. This is a bug-fix
release.
