---
title:    "Mesa 23.0.4 is released"
date:     2023-05-30
category: releases
tags:     []
---
[Mesa 23.0.4](https://docs.mesa3d.org/relnotes/23.0.4.html) is released.
This is a bug fix release.
