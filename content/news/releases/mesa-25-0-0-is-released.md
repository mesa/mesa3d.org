---
title:    "Mesa 25.0.0 is released"
date:     2025-02-19
category: releases
tags:     []
---
[Mesa 25.0.0](https://docs.mesa3d.org/relnotes/25.0.0.html) is released.
This is a new development release. See the release notes for more information about this release.
