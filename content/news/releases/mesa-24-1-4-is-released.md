---
title:    "Mesa 24.1.4 is released"
date:     2024-07-17
category: releases
tags:     []
---
[Mesa 24.1.4](https://docs.mesa3d.org/relnotes/24.1.4.html) is released.
This is a bug fix release.
