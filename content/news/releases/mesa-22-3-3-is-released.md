---
title:    "Mesa 22.3.3 is released"
date:     2023-01-11
category: releases
tags:     []
---
[Mesa 22.3.3](https://docs.mesa3d.org/relnotes/22.3.3.html) is released.
This is a bug fix release.
