---
title:    "Mesa 23.3.2 is released"
date:     2023-12-27
category: releases
tags:     []
---
[Mesa 23.3.2](https://docs.mesa3d.org/relnotes/23.3.2.html) is released.
This is a bug fix release.
