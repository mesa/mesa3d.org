---
title:    "Mesa 24.2.2 is released"
date:     2024-09-05
category: releases
tags:     []
---
[Mesa 24.2.2](https://docs.mesa3d.org/relnotes/24.2.2.html) is released.
This is a bug fix release.
