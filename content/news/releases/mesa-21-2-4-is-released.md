---
title:    "Mesa 21.2.4 is released"
date:     2021-10-14
category: releases
tags:     []
---
[Mesa 21.2.4](https://docs.mesa3d.org/relnotes/21.2.4.html) is released. This is a bug fix release.
