---
title:    "Mesa 24.3.1 is released"
date:     2024-12-04
category: releases
tags:     []
---
[Mesa 24.3.1](https://docs.mesa3d.org/relnotes/24.3.1.html) is released.
This is a bug fix release.
