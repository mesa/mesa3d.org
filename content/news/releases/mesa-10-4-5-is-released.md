---
title:    "Mesa 10.4.5 is released"
date:     2015-02-21
category: releases
tags:     []
---
[Mesa 10.4.5](https://docs.mesa3d.org/relnotes/10.4.5.html) is released. This is a bug-fix
release.
