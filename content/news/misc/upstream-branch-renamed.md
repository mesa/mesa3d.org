---
title:    "Upstream branch renamed"
date:     2021-05-13 11:20:00
category: misc
tags:     []
---
We recently renamed our upstream development branch from "master" to
"main". Developers might want to [update] their upstream branches on
their end as well.

You can read more about this on the mailing list [announcement].

[update]: https://gitlab.freedesktop.org/mesa/mesa/-/commit/a1c56b80915a1105c5a62aa6ff3ac71c1edd143d
[announcement]: https://lists.freedesktop.org/archives/mesa-dev/2021-May/225247.html
