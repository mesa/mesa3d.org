---
title:    "Mesa 24.3.4 is released"
date:     2025-01-22
category: releases
tags:     []
---
[Mesa 24.3.4](https://docs.mesa3d.org/relnotes/24.3.4.html) is released.
This is a bug fix release.
