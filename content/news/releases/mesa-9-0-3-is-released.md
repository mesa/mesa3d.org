---
title:    "Mesa 9.0.3 is released"
date:     2013-02-21
category: releases
tags:     []
---
[Mesa 9.0.3](https://docs.mesa3d.org/relnotes/9.0.3.html) is released. This is a bug fix
release.
