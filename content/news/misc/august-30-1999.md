---
title:    "August 30, 1999"
date:     1999-08-30 00:00:00
category: misc
tags:     []
summary:  "I'm pleased to announce that I've accepted a position with Precision
Insight, Inc. effective October, 1999. I'll be leaving Avid Technology
in September."
---
I'm pleased to announce that I've accepted a position with Precision
Insight, Inc. effective October, 1999. I'll be leaving Avid Technology
in September.

I've been working on Mesa in my spare time for over five years. With
Precision Insight I now have the opportunity to devote my full attention
to advancing Mesa and OpenGL on Linux.

While I'll be focused on Linux, the X Window System, and hardware
acceleration, my work will continue to be open sourced and available to
any other programmers who may want to contribute to it, or use it for
other projects or platforms

PS: I'm going to be traveling until Sep 6 and won't be reading email
until then.
