---
title:    "Mesa 6.5 has been released"
date:     2006-03-31
category: releases
tags:     []
---
[Mesa 6.5](https://docs.mesa3d.org/relnotes/6.5.html) has been released. This is a new
development release.
