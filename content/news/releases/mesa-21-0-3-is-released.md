---
title:    "Mesa 21.0.3 is released"
date:     2021-04-21
category: releases
tags:     []
---
[Mesa 21.0.3](https://docs.mesa3d.org/relnotes/21.0.3.html) is released. This is a bug fix release.
