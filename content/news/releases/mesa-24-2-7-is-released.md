---
title:    "Mesa 24.2.7 is released"
date:     2024-11-13
category: releases
tags:     []
---
[Mesa 24.2.7](https://docs.mesa3d.org/relnotes/24.2.7.html) is released.
This is a bug fix release.
