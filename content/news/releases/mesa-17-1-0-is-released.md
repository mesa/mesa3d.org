---
title:    "Mesa 17.1.0 is released"
date:     2017-05-10
category: releases
tags:     []
---
[Mesa 17.1.0](https://docs.mesa3d.org/relnotes/17.1.0.html) is released. This is a new
development release. See the release notes for more information about
the release.
