---
title:    "Mesa 24.0.7 is released"
date:     2024-05-08
category: releases
tags:     []
---
[Mesa 24.0.7](https://docs.mesa3d.org/relnotes/24.0.7.html) is released.
This is a bug fix release.
