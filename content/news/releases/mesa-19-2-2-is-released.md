---
title:    "Mesa 19.2.2 is released"
date:     2019-10-24
category: releases
tags:     []
---
[Mesa 19.2.2](https://docs.mesa3d.org/relnotes/19.2.2.html) is released. This is a bug fix
release.
