---
title:    "Mesa 22.0.5 is released"
date:     2022-06-01
category: releases
tags:     []
---
[Mesa 22.0.5](https://docs.mesa3d.org/relnotes/22.0.5.html) is released. This is a bug fix release.
