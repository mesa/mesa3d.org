---
title:    "Mesa 22.3.5 is released"
date:     2023-02-08
category: releases
tags:     []
---
[Mesa 22.3.5](https://docs.mesa3d.org/relnotes/22.3.5.html) is released.
This is a bug fix release.
