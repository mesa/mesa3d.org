---
title:    "Mesa 24.3.2 is released"
date:     2024-12-19
category: releases
tags:     []
---
[Mesa 24.3.2](https://docs.mesa3d.org/relnotes/24.3.2.html) is released.
This is a bug fix release.
