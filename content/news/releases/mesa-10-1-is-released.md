---
title:    "Mesa 10.1 is released"
date:     2014-03-04
category: releases
tags:     []
---
[Mesa 10.1](https://docs.mesa3d.org/relnotes/10.1.html) is released. This is a new development
release. See the release notes for more information about the release.
