---
title:    "Mesa 24.0.8 is released"
date:     2024-05-22
category: releases
tags:     []
---
[Mesa 24.0.8](https://docs.mesa3d.org/relnotes/24.0.8.html) is released.
This is a bug fix release.
