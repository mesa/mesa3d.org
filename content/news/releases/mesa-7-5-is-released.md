---
title:    "Mesa 7.5 is released"
date:     2009-07-17
category: releases
tags:     []
---
[Mesa 7.5](https://docs.mesa3d.org/relnotes/7.5.html) is released. This is a new features
release. People especially concerned about stability may want to wait
for the follow-on 7.5.1 bug-fix release.
