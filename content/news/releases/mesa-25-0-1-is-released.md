---
title:    "Mesa 25.0.1 is released"
date:     2025-03-05
category: releases
tags:     []
---
[Mesa 25.0.1](https://docs.mesa3d.org/relnotes/25.0.1.html) is released.
This is a bug fix release.
