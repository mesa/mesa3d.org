---
title:    "Mesa 24.2.6 is released"
date:     2024-10-30
category: releases
tags:     []
---
[Mesa 24.2.6](https://docs.mesa3d.org/relnotes/24.2.6.html) is released.
This is a bug fix release.
