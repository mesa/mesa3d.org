---
title:    "Mesa 10.2.5 is released"
date:     2014-08-02
category: releases
tags:     []
---
[Mesa 10.2.5](https://docs.mesa3d.org/relnotes/10.2.5.html) is released. This is a bug-fix
release.
