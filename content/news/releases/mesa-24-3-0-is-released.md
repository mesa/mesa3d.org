---
title:    "Mesa 24.3.0 is released"
date:     2024-11-21
category: releases
tags:     []
---
[Mesa 24.3.0](https://docs.mesa3d.org/relnotes/24.3.0.html) is released.
This is a new development release. See the release notes for more information about this release.
