---
title:    "Mesa 22.0.0 is released"
date:     2022-03-09
category: releases
tags:     []
---
[Mesa 22.0.0](https://docs.mesa3d.org/relnotes/22.0.0.html) is released. This is a new development release. See the release notes for more information about this release.
