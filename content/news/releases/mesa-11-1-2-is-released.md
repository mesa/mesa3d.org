---
title:    "Mesa 11.1.2 is released"
date:     2016-02-10
category: releases
tags:     []
---
[Mesa 11.1.2](https://docs.mesa3d.org/relnotes/11.1.2.html) is released. This is a bug-fix
release.
