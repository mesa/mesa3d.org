---
title:    "Mesa 24.1.7 is released"
date:     2024-08-29
category: releases
tags:     []
---
[Mesa 24.1.7](https://docs.mesa3d.org/relnotes/24.1.7.html) is released.
This is a bug fix release.
