---
title:    "Mesa 24.2.8 is released"
date:     2024-11-28
category: releases
tags:     []
---
[Mesa 24.2.8](https://docs.mesa3d.org/relnotes/24.2.8.html) is released.
This is a bug fix release.
