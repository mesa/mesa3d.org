---
title:    "Mesa 24.2.4 is released"
date:     2024-10-03
category: releases
tags:     []
---
[Mesa 24.2.4](https://docs.mesa3d.org/relnotes/24.2.4.html) is released.
This is a bug fix release.
