---
title:    "Mesa 24.0.9 is released"
date:     2024-06-06
category: releases
tags:     []
---
[Mesa 24.0.9](https://docs.mesa3d.org/relnotes/24.0.9.html) is released.
This is a bug fix release.
