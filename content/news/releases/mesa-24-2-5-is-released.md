---
title:    "Mesa 24.2.5 is released"
date:     2024-10-16
category: releases
tags:     []
---
[Mesa 24.2.5](https://docs.mesa3d.org/relnotes/24.2.5.html) is released.
This is a bug fix release.
