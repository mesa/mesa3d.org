---
title:    "Mesa 10.6.9 is released"
date:     2015-10-03
category: releases
tags:     []
summary:  "[Mesa 10.6.9](https://docs.mesa3d.org/relnotes/10.6.9.html) is released. This is a bug-fix
release."
---
[Mesa 10.6.9](https://docs.mesa3d.org/relnotes/10.6.9.html) is released. This is a bug-fix
release.

{{< alert type="info" title="Note" >}}
It is anticipated that 10.6.9 will be the final release in the
10.6 series. Users of 10.6 are encouraged to migrate to the 11.0 series
in order to obtain future fixes.
{{< /alert >}}
