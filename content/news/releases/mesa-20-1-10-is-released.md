---
title:    "Mesa 20.1.10 is released"
date:     2020-10-14
category: releases
tags:     []
---
[Mesa 20.1.10](https://docs.mesa3d.org/relnotes/20.1.10.html) is released.

{{< alert type="info" title="Note" >}}
This is the last bug fix release for the 20.1 series. Users are advised to
update to the 20.2 series.
{{< /alert >}}
