---
title:    "Mesa 12.0.3 is released"
date:     2016-09-15
category: releases
tags:     []
---
[Mesa 12.0.3](https://docs.mesa3d.org/relnotes/12.0.3.html) is released. This is a bug-fix
release.
