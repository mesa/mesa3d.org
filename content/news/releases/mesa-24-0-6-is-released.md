---
title:    "Mesa 24.0.6 is released"
date:     2024-04-24
category: releases
tags:     []
---
[Mesa 24.0.6](https://docs.mesa3d.org/relnotes/24.0.6.html) is released.
This is a bug fix release.
