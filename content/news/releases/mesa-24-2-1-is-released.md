---
title:    "Mesa 24.2.1 is released"
date:     2024-08-28
category: releases
tags:     []
---
[Mesa 24.2.1](https://docs.mesa3d.org/relnotes/24.2.1.html) is released.
This is a bug fix release.
