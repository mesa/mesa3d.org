---
title:    "Mesa 18.0.5 is released"
date:     2018-06-03
category: releases
tags:     []
summary:  "[Mesa 18.0.5](https://docs.mesa3d.org/relnotes/18.0.5.html) is released. This is a bug-fix
release."
---
[Mesa 18.0.5](https://docs.mesa3d.org/relnotes/18.0.5.html) is released. This is a bug-fix
release.

{{< alert type="info" title="Note" >}}
It is anticipated that 18.0.5 will be the final release in the
18.0 series. Users of 18.0 are encouraged to migrate to the 18.1 series
in order to obtain future fixes.
{{< /alert >}}
