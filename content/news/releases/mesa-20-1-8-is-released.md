---
title:    "Mesa 20.1.8 is released"
date:     2020-09-16
category: releases
tags:     []
---
[Mesa 20.1.8](https://docs.mesa3d.org/relnotes/20.1.8.html) is released. This is a bug fix release.
