---
title:    "Mesa 24.0.4 is released"
date:     2024-03-27
category: releases
tags:     []
---
[Mesa 24.0.4](https://docs.mesa3d.org/relnotes/24.0.4.html) is released.
This is a bug fix release.
