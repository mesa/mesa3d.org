---
title:    "Mesa 24.1.3 is released"
date:     2024-07-03
category: releases
tags:     []
---
[Mesa 24.1.3](https://docs.mesa3d.org/relnotes/24.1.3.html) is released.
This is a bug fix release.
