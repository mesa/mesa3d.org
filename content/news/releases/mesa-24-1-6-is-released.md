---
title:    "Mesa 24.1.6 is released"
date:     2024-08-14
category: releases
tags:     []
---
[Mesa 24.1.6](https://docs.mesa3d.org/relnotes/24.1.6.html) is released.
This is a bug fix release.
