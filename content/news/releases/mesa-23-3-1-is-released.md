---
title:    "Mesa 23.3.1 is released"
date:     2023-12-13
category: releases
tags:     []
---
[Mesa 23.3.1](https://docs.mesa3d.org/relnotes/23.3.1.html) is released.
This is a bug fix release.
