---
title:    "Mesa demos 8.1.0 is released"
date:     2013-02-24
category: releases
tags:     [mesa-demos]
---
Mesa demos 8.1.0 is released. See the
[announcement](https://lists.freedesktop.org/archives/mesa-dev/2013-February/035180.html)
for more information about the release. You can download it from
[ftp.freedesktop.org/pub/mesa/demos/8.1.0/](ftp://ftp.freedesktop.org/pub/mesa/demos/8.1.0/).
